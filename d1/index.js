console.log("Hello World!");

// JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables us to create dynamically udpating content, control multimedia, and animate images.

/*
Syntax, Statements, and Comments

• Statements
- Statements in programming are instructions that we tell the computer to perform.
*JS statements usually end with a semicolon(;)
*Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends.

• Syntax
- Syntax in programming, it is the set of rules that describes how statements must be formed or constructed.

• Comments
- Comments are parts of the code that gets ignored by the language.
- Comments are meant to describe the written code.

There are two types of comments:
1. The single-line commend (ctrl + /)
	- denoted by two slashes
2. The multi-line comment (ctrl + shift + /)
	- denoted by a slash and asterisk

*/

/*

// --- Variables --- //
- it is used to contain our data.
- This makes it easier for us to associate information stored in our devices to actual "names" about information.

• Declaring variables
- tells our devices that a variable name is created and is ready to store data.

Two Syntaxes of declaring variables:
let variableName;
const variableName;
*/

let myVariable;
console.log(myVariable);
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined.
// console.log() is useful for printing values of variables or certain results of code into the browser's console. *Constant use of this throughout developing an application will save us time and builds good habits in always checking for the output of our code.

let hello;
console.log(hello); //undefined
// Variables must be declared first before they are used.
// Using variables before they are declared will return an error.

/*

Guides in writing variables:
	1. Use the 'let' keyword folloed by the variable name of your choosing and use the assignment operator '=' to assign a value.
	2. Variable names should start with a lowercase character then the following words should start in uppercase character, this type of writing words is called "camelCase".
	3. For constant variables, use the 'const' keyword.
	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

*/

/*

Declare and initialize variables
- Initializing variables - the instance when a variable is given its initial or starting value.

Syntax:
let variable = value;

*/

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539; //details/value that should not be changed shall use 'const'
// const pi = 3.1416;

//Reassigning variable values
//- reassigning a variable means changing its initial or previous value into another value
	//Syntax:
	//variableName = newValue;
productName = "Laptop";
console.log(productName);

//let variable cannot be re-declared within its scope so this will work:
// let friend = "Kate";
// friend = "Nedj";
// console.log(friend);

// let friend = "Kate";
// let friend = "Nej"; //This will return an error
//error: indentifier 'friend' has already been declared

//const variables cannot be re-assigned.
// interest = 4.489;
// console.log(interest);

//Reassigning variables vs initializing variables
//declare a variable first

	let supplier;
	supplier = "John Smith Tradings";
	//Initialization is done after the variable has been declared
	//This is considered as initialization because it is the first time that a value has been assigned to a variable
	console.log(supplier);

	//considered as reassignment because its initial value has already been declared
	supplier = "Zuitt Store";
	console.log(supplier);

	//we cannot declare a const variable without initialization
	//const pi;
	//pi = 3.1416;
	//console.log("pi"); //error

/*

var
var is also used in declaring a variable
let/const were introduced as a new feature in E26 (2015)

*/

// a = 5;
// console.log(a); //output: 5
// var a;

//Hoisting is JS's default behavior of moving declarations to the top
//In terms of variables and constants, keyword var is hoisted, and let and const does not allow hoisting.

// a = 5;
// console.log(a);
// let a;

/*

let/const local/global scope

Scope essentially means where these variables are available for use
//let and const are blocked-scope
//block is a chunk of code bounded by {}. A block live in curly braces. Anything within curly braces is a block.
//So a variable declared in a block with let/const is only available for use within that block.

*/

let outerVariable = "Hello";
{
	let innerVariable = "Hello again";
	console.log(innerVariable);
}
console.log(outerVariable);
//console.log(innerVariable); //innerVariable is not defined

/*

Multiple variable declarations
- multiple variables may be declared in one line
- although it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let/const" keywords when declaring each variable.
- using multiple keywords makes code easier to read and determine what kind of variable has been created.

*/

//let productCode = "DC017", productBrand = "Dell";
let productCode = "DC017";
let productBrand = "Dell";
console.log(productCode, productBrand); //output: DC017

//Using a variable with a reserved keyword
// const let = "Hello";
// console.log(let); //error

/*

Data Types:
1. Strings
- a series of characters that create a word, phrase, sentence, or anything related to creating text.
- Strings in JavaScript can be written using either a single-quotation marks('') or double quotation marks("").
*In other programming languages, only the double quotation marks can be used for creating strings.

*/

let country = 'Philippines';
let province = "Metro Manila";

/*

Concatenating strings
- multiple string values can be combined to create a single string using the "+" symbol

*/

let fullAddress = province + ", " + country;
console.log(fullAddress); //output: Metro Manila, Philippines

let greeting = "I Live in the " + country;
console.log(greeting); //output: I live in the Philippines

/*

The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

*/

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress); 
/*
output:
Metro Manila

Philippines
*/

//Using the double quotation marks along with single quotation marks can allow us to easily include single quotes in texts without using the escape character

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

/*

2. Numbers
	
*/

// •Integers/Whole Numbers
let headCount = 26;
console.log(headCount); //output: 26

// •Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade); //output: 98.7

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance); //output: 20000000000

//Combining strings/text and numbers
console.log("John's grade last quarter is " + grade); 
//output: John's grade last quarter is 98.7

// •Boolean
//	- boolean values are normally used to store values relating to the state of certain things.
// - this will be useful in furthur discussion about creating logic to make our application respond to certain situations/scenarios.

let isMarried = false;
let isGoodConduct = true;

console.log("is Married: " + isMarried);
console.log("is Good Conduct: " + isGoodConduct);

// •Arrays
//	- are a special kind of data type that is used to store multiple values
//	- arrays can store different data types but is normally used to store similar data types.

//similar data types
//Syntax
	//let/cont arrayName = [elementA, elementB, elementC, ...];

	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);
	
	//different data types
	//storing different data types inside an array is NOT RECOMMENDED because it will not make sense in the context of programming
	let details = ["John", "Smith", 32, true];
	console.log(details);

// •Objects
//	- objects are another special kind of data type that's used to mimic real world objects/items
//	- they're used to create complex data that contains pieces of information that are relevant to each other
//	- every individual priece of information is called a property of the object

//Syntax
	//let/const objectName = {
		//propertyA: value,
		//propertyB: value
	//}

	let person = {
		fullName: "Edward Scissorhands",
		age: 25,
		isMarried: false,
		contact: ["+63171234567", "8123 4567"],
		address: {
			houseNumber: '345',
			city: "Manila",
		}
	}
	console.log(person);

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}
	console.log(myGrades);
	//Objects will be displayed in alphabetical/ascending order in the browser console

	console.log(typeof myGrades); //output: object
	console.log(typeof grades); //output: object
	//note: array is a special type of object with methods and functions to manipulate it
	//We will discuss these methods in later sessions (s22 - Array Manipulation)

	const anime = ["OP", "OPM", "AOT", "BNHA"];
	anime[0] = 'JJK';
	console.log(anime);
	//we can reassign const array values

// •Null
//	- simply means that data type was assigned to a variable but it does not hold any value/amount or is nullified.
	let spouse = null;
	console.log(spouse);
	//null is also considered a data type of its own compared to 0 which is a NUMBER and single quotes which are a data type of a String

	let myNumber = 0; //output: 0
	let myString = ''; //output: 
	console.log(myNumber);
	console.log(myString);

// •Undefined
//	-represents the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName); //output: undefined

	//one clear difference between undefined and null is that for undefined, a variable was created but was not provided a value but for null, it was provided with a 'null' value.